package com.example.student.customnotification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RemoteViews;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button=(Button)findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buildNotification();
            }
        });
    }

    protected void buildNotification() {

        // Open NotificationView.java Activity
        /*PendingIntent pIntent = PendingIntent.getActivity(
                getContext(),
                NOTIFICATION_ID,
                getIntent(),
                PendingIntent.FLAG_UPDATE_CURRENT);*/

        NotificationCompat.Builder builder = new NotificationCompat.Builder(MainActivity.this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker("Hello Ticker")
                .setContentTitle(getTitle())
                .setContentText("Hello Text")
                .setSmallIcon(android.R.drawable.ic_menu_gallery)
                .setAutoCancel(true)
                .setColor(getResources().getColor(R.color.colorPrimaryDark));
                // Set PendingIntent into Notification
                //.setContentIntent(pIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            // build a complex notification, with buttons and such
            //
            //builder = builder.setContent(notification());
        } else {
            // Build a simpler notification, without buttons
            //
           /* builder = builder.setContentTitle(getTitle())
                    .setContentText("Hello Text")
                    .setSmallIcon(android.R.drawable.ic_menu_gallery);*/
        }
        NotificationManager nm=(NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        Notification notification=builder.build();
        notification.bigContentView=notification();
        nm.notify(0,notification);
    }

    private RemoteViews notification(){
        RemoteViews remoteViews=new RemoteViews(getPackageName(),R.layout.custom);

        remoteViews.setTextViewText(R.id.tx1,"Place Bid");
        remoteViews.setTextViewText(R.id.shipment_data,"MR1234567890");
        remoteViews.setTextViewText(R.id.product_data,"Pipe");
        remoteViews.setTextViewText(R.id.from_data,"Jaipur");
        remoteViews.setTextViewText(R.id.to_data,"Jaipur");
        remoteViews.setTextViewText(R.id.pickup_date_data,"10-2-16");
        remoteViews.setTextViewText(R.id.weight_data,"10 ton");
        remoteViews.setTextViewText(R.id.truck_type__data,"Small commercial truck");
        remoteViews.setTextViewText(R.id.remark_data,"2 Feet height");

        remoteViews.setImageViewResource(R.id.img1,R.mipmap.ic_launcher);
        remoteViews.setImageViewResource(R.id.img2,R.drawable.ic_highlight_off_black_24dp);

        return remoteViews;
    }
}
